package com.example.petfinder4goon;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petfinder4goon.Api.ApiClient;
import com.example.petfinder4goon.Api.ApiService;
import com.example.petfinder4goon.Model.AnimalItem;
import com.example.petfinder4goon.Model.Animals;
import com.example.petfinder4goon.Model.OneAnimal;
import com.example.petfinder4goon.Model.POJO.Attributes;
import com.example.petfinder4goon.Model.POJO.Breeds;
import com.example.petfinder4goon.Model.POJO.Colors;
import com.example.petfinder4goon.Model.POJO.Photos;
import com.example.petfinder4goon.PetList.PetListActivity;
import com.example.petfinder4goon.PetList.PetsAdapter;
import com.example.petfinder4goon.Utilities.DownloadImageTask;
import com.example.petfinder4goon.Utilities.SharedPrefs;
import com.example.petfinder4goon.Utilities.Utilitis;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PetDetailsActivity extends AppCompatActivity {

    private int id;
    private AnimalItem animal;
    TextView nameTextView, genderTextView, ageTextView, sizeTextView, breedsTextView, coatTextView;
    TextView attrTextView, descTextView, statusTextView;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_details);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id = extras.getInt("id");
            Log.v("petIDdetails",String.valueOf(id));

        }

        Toolbar toolbar = findViewById(R.id.toolbar_petlist);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        ImageButton btn = findViewById(R.id.details_backbutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PetDetailsActivity.super.onBackPressed();
            }
        });

        nameTextView = findViewById(R.id.d_animal_name);
        ageTextView = findViewById(R.id.d_age);
        genderTextView = findViewById(R.id.d_gender);
        sizeTextView = findViewById(R.id.d_size);
        image = findViewById(R.id.d_petpic);
        image.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.lightPrimary));
        breedsTextView = findViewById(R.id.d_breeds);
        coatTextView = findViewById(R.id.d_coat);
        attrTextView = findViewById(R.id.d_attr);
        descTextView = findViewById(R.id.d_desc);
        statusTextView = findViewById(R.id.d_status);

        // method to check if the token is valid
        Utilitis.RefreshToken();
        GetAnimal(id);

    }

    private void GetAnimal(int id){
        LinearLayout progressBar = findViewById(R.id.llProgressBar_petdetails);
        progressBar.setVisibility(View.VISIBLE);
        ApiService api = ApiClient.getClient().create(ApiService.class);
        Call<OneAnimal> call = api.getAnimal("Bearer " + SharedPrefs.instance().fetchValueString("token"), id);
        call.enqueue(new Callback<OneAnimal>() {
            @Override
            public void onResponse(Call<OneAnimal> call, Response<OneAnimal> response) {
                if(response.isSuccessful()){
                    //Toast.makeText(PetListActivity.this, "successful", Toast.LENGTH_SHORT).show();
                    OneAnimal ResponseJson = response.body();

                    animal = ResponseJson.getAnimalItems();

                    Log.v("namepet", animal.getName());

                    if(animal != null) {
                        nameTextView.setText(animal.getName());
                        ageTextView.setText(animal.getAge());
                        genderTextView.setText(animal.getGender());
                        sizeTextView.setText(animal.getSize());
                        setBreeds(animal.getBreeds(),breedsTextView);
                        coatTextView.setText("COAT LENGTH: " + animal.getCoat());
                        if(animal.getDescription() == null || animal.getDescription().isEmpty()){
                            TextView tv = findViewById(R.id.textView10);
                            tv.setVisibility(View.GONE);
                        }
                        descTextView.setText(animal.getDescription());
                        setAttr(animal.getAttributes(), attrTextView);
                        statusTextView.setText("Status: " + animal.getStatus());

                        if(animal.getPhotos().length > 0 ){
                            String url = animal.getPhotos()[0].getMeduim();
                            new DownloadImageTask(image).execute(url);
                            progressBar.setVisibility(View.GONE);
                        }
                        else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OneAnimal> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), R.string.connection_problem, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setBreeds(Breeds breeds, TextView textView){
        if(breeds.getPrimary()!=null){
            textView.setText("BREEDS: " + breeds.getPrimary());
            if(breeds.getSecondry()!=null){
                textView.append(", " + breeds.getSecondry());
            }
            textView.append("\nMIXED: " +  (breeds.getMixed()));
        }
    }

    @SuppressLint("SetTextI18n")
    private void setAttr(Attributes attr, TextView textView){
        if(attr.getDeclawed()!=null){
            textView.setText("DECLAWED: " + (attr.getDeclawed())+"   ");
        }
        if (attr.getHouse_trained()!=null){
            textView.setText("HOUSE-TRAINED: " + attr.getHouse_trained()+"   ");
        }
        if(attr.getSpayed_neutered()!=null){
            textView.setText("NEUTERED: " + attr.getSpayed_neutered());
        }
        if(attr.getSpecial_needs()!=null){
            textView.setText("\nSPECIAL NEED: " + attr.getSpecial_needs()+"   ");
        }
        if(attr.getShots_current()!=null){
            textView.setText("SHOTS CURRENT: " + attr.getSpayed_neutered());
        }

    }
}