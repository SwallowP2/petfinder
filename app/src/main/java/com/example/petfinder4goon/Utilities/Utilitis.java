package com.example.petfinder4goon.Utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.example.petfinder4goon.Api.ApiClient;
import com.example.petfinder4goon.Api.ApiService;
import com.example.petfinder4goon.Api.tokenResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utilitis {
    private static String client_id = "6Iq2lWQZxtt77vrEiJFanPLweVXXl2tESTnj1GPBwWCPEgkI40";
    private static String client_secret = "toYNn9Kn31uAlSWzoICmbuwMdEp3zGEdOX7x2BaW";
    private static String token = null;

    public static String getClient_id() {
        return client_id;
    }

    public static String getClient_secret() {
        return client_secret;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        Utilitis.token = token;
    }

    public static void setClient_id(String client_id) {
        Utilitis.client_id = client_id;
    }

    public static void setClient_secret(String client_secret) {
        Utilitis.client_secret = client_secret;
    }

    public static void RefreshToken(){
        try{
            int exipreTime = (SharedPrefs.instance().fetchValueInt("expires_in")) * 1000;
            long currentTime = System.currentTimeMillis();
            long creationTime = SharedPrefs.instance().fetchValueInt("token_creation_time");

            if (currentTime - creationTime > exipreTime){
                ApiService api = ApiClient.getClient().create(ApiService.class);
                Call<ResponseBody> call = api.getToken("client_credentials", client_id
                        ,client_secret);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Log.v("refreshToken", "new token fetched");
                            String ResponseJson = null;
                            try {
                                ResponseJson = response.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Gson gson = new Gson();
                            tokenResponse resp = gson.fromJson(ResponseJson, tokenResponse.class);
                            Utilitis.setToken(resp.getAcccess_token());
                            SharedPrefs.instance().storeValueString("token",resp.getAcccess_token());
                            SharedPrefs.instance().storeValueInt("expires_in",resp.getExpires_in());
                            SharedPrefs.instance().storeValueInt("token_creation_time", (int) System.currentTimeMillis());
                        }
                        else {
                            Log.v("refreshToken", "something went wrong");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.v("refreshToken", "connection problem");
                    }
                });
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
