package com.example.petfinder4goon.Utilities;

import android.app.Activity;
import android.content.SharedPreferences;


public class SharedPrefs {

    Activity activity;
    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedPrefEditor;
    static SharedPrefs _instance;

    public SharedPrefs(){

    }

    public static SharedPrefs instance() {
        return _instance;
    }

    public static SharedPrefs instance(Activity activity) {
        if (_instance == null) {
            _instance = new SharedPrefs();
            _instance.configSessionUtils(activity);
        }
        return _instance;
    }


    // create sharePrefs method
    public void configSessionUtils(Activity context) {
        activity = context;
        sharedPref = context.getSharedPreferences(Utilitis.getToken(), Activity.MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.apply();
    }

    // put data in sharePrefs method
    public void storeValueString(String key, String value) {
        sharedPrefEditor.putString(key, value);
        sharedPrefEditor.commit();
    }

    public void storeValueInt(String key, int value){
        sharedPrefEditor.putInt(key,value);
        sharedPrefEditor.commit();
    }

    public void storeValueLong(String key, Long value){
        sharedPrefEditor.putLong(key,value);
        sharedPrefEditor.commit();
    }

    // delete all data in sharePrefs
    public void clearPref() {
        sharedPrefEditor.remove("token");
        sharedPrefEditor.commit();
    }

    // Fetch data from sharePrefs
    public String fetchValueString(String key) {
        return sharedPref.getString(key, null);
    }

    public int fetchValueInt(String key) {
        return sharedPref.getInt(key, 0);
    }

    public Long fetchValueLong(String key) {
        return sharedPref.getLong(key, 0);
    }
}
