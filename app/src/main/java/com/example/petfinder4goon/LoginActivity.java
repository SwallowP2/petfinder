package com.example.petfinder4goon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petfinder4goon.Api.ApiClient;
import com.example.petfinder4goon.Api.ApiService;
import com.example.petfinder4goon.Api.tokenResponse;
import com.example.petfinder4goon.PetList.PetListActivity;
import com.example.petfinder4goon.Utilities.SharedPrefs;
import com.example.petfinder4goon.Utilities.Utilitis;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    EditText usernameTextField;
    EditText passwordTextField;
    TextView appnameTextview;
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPrefs.instance(LoginActivity.this);

        // check if user has signed in
        if ((SharedPrefs.instance().fetchValueString("token")) != null) {
            startActivity(new Intent(LoginActivity.this, PetListActivity.class));
            finishAffinity();
        }

        // check screen orientation to configure UI
        if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            appnameTextview = findViewById(R.id.textView_appname);
            appnameTextview.setVisibility(View.GONE);
        }

        usernameTextField = findViewById(R.id.usernameInputEditText);
        usernameTextField.setText(Utilitis.getClient_id());
        passwordTextField = findViewById(R.id.passwordInputEditText);
        passwordTextField.setText(Utilitis.getClient_secret());
        loginBtn = findViewById(R.id.loginButton);

        loginBtn.setOnClickListener(v -> {
            if (usernameTextField.getText().toString().isEmpty() || passwordTextField.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), (R.string.text_check_faild_toast), Toast.LENGTH_SHORT).show();
            } else {
                Login();
            }
        });

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            appnameTextview = findViewById(R.id.textView_appname);
            appnameTextview.setVisibility(View.GONE);
        }
        else if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            appnameTextview = findViewById(R.id.textView_appname);
            appnameTextview.setVisibility(View.VISIBLE);
        }
    }

    private void Login() {
        LinearLayout progressBar = findViewById(R.id.llProgressBar);
        progressBar.setVisibility(View.VISIBLE);
        try
        {
            String username = usernameTextField.getText().toString();
            String password = passwordTextField.getText().toString();

            ApiService api = ApiClient.getClient().create(ApiService.class);
            Call<ResponseBody> call = api.getToken("client_credentials", username,password);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        progressBar.setVisibility(View.GONE);
                        try {
                            String ResponseJson = response.body().string();
                            Gson gson = new Gson();
                            tokenResponse resp = gson.fromJson(ResponseJson, tokenResponse.class);
                            Utilitis.setToken(resp.getAcccess_token());

                            Intent intent = new Intent(LoginActivity.this, PetListActivity.class);
                            startActivity(intent);
                            finish();
                            // save token to shared prefs
                            SharedPrefs.instance().storeValueString("token", resp.getAcccess_token());
                            SharedPrefs.instance().storeValueInt("expires_in",resp.getExpires_in());
                            SharedPrefs.instance().storeValueLong("token_creation_time", System.currentTimeMillis());
                            SharedPrefs.instance().storeValueString("client_id",username);
                            SharedPrefs.instance().storeValueString("client_secret",password);

                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, R.string.something_went_wrong + ": " + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "Worng username or password", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, R.string.connection_problem, Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception e){
            progressBar.setVisibility(View.GONE);
            e.printStackTrace();
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}