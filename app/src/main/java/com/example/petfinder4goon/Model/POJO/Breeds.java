package com.example.petfinder4goon.Model.POJO;

import com.google.gson.annotations.SerializedName;

public class Breeds {
    @SerializedName("primary")
    private String primary;
    @SerializedName("secondary")
    private String secondry;
    @SerializedName("mixed")
    private boolean mixed;

    public String getPrimary() {
        return primary;
    }

    public String getSecondry() {
        return secondry;
    }

    public boolean getMixed(){
        return mixed;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public void setSecondry(String secondry) {
        this.secondry = secondry;
    }

    public void setMixed(boolean mixed) {
        this.mixed = mixed;
    }
}
