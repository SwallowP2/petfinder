package com.example.petfinder4goon.Model;

import com.example.petfinder4goon.Model.POJO.Type;
import com.google.gson.annotations.SerializedName;

public class AnimalTypes {
    @SerializedName("types")
    private Type[] types;

    public AnimalTypes(Type[] type){
        this.types = type;
    }

    public Type[] getTypes() {
        return types;
    }
}
