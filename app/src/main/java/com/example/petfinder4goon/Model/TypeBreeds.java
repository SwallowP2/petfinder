package com.example.petfinder4goon.Model;

import com.example.petfinder4goon.Model.POJO.Type;
import com.google.gson.annotations.SerializedName;

public class TypeBreeds {
    @SerializedName("breeds")
    private Type[] breeds;

    public Type[] getBreeds() {
        return breeds;
    }
}
