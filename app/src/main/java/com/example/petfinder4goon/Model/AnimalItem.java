package com.example.petfinder4goon.Model;

import androidx.annotation.DimenRes;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.petfinder4goon.Model.POJO.Attributes;
import com.example.petfinder4goon.Model.POJO.Breeds;
import com.example.petfinder4goon.Model.POJO.Colors;
import com.example.petfinder4goon.Model.POJO.Photos;
import com.google.gson.annotations.SerializedName;

@Entity
public class AnimalItem {

    public AnimalItem(int id, String name, String description, String age, String type, String size, String gender, String coat, String status, String url,
                      Photos[] photos, Photos croppedPhoto, Colors colors, Breeds breeds, Attributes attributes) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.age = age;
        this.type = type;
        this.size = size;
        this.gender = gender;
        this.coat = coat;
        this.status = status;
        this.url = url;
        this.photos = photos;
        this.croppedPhoto = croppedPhoto;
        this.colors = colors;
        this.breeds = breeds;
        this.attributes = attributes;
    }

    public AnimalItem(int id, String name, String description, String age, String type, String size, String gender, String coat, String status, String url) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.age = age;
        this.type = type;
        this.size = size;
        this.gender = gender;
        this.coat = coat;
        this.status = status;
        this.url = url;
    }

    @SerializedName("id")
    @PrimaryKey
    private int id;
    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;
    @SerializedName("description")
    @ColumnInfo(name = "description")
    private String description;
    @SerializedName("age")
    @ColumnInfo(name = "age")
    private String age;
    @SerializedName("type")
    @ColumnInfo(name = "type")
    private String type;
    @SerializedName("size")
    @ColumnInfo(name = "size")
    private String size;
    @SerializedName("gender")
    @ColumnInfo(name = "gender")
    private String gender;
    @SerializedName("coat")
    @ColumnInfo(name = "coat")
    private String coat;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;
    @SerializedName("url")
    @ColumnInfo(name = "url")
    private String url;

    @SerializedName("photos")
    @Ignore
    private Photos[] photos;
    @SerializedName("primary_photo_cropped")
    @Ignore
    private Photos croppedPhoto;
    @SerializedName("colors")
    @Ignore
    private Colors colors;
    @SerializedName("breeds")
    @Ignore
    private Breeds breeds;
    @SerializedName("attributes")
    @Ignore
    private Attributes attributes;


    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getGender() {
        return gender;
    }

    public String getCoat() {
        return coat;
    }

    public String getAge() {
        return age;
    }

    public String getSize() {
        return size;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Photos[] getPhotos() {
        return photos;
    }

    public Photos getCroppedPhoto() {
        return croppedPhoto;
    }

    public Colors getColors() {
        return colors;
    }

    public Breeds getBreeds() {
        return breeds;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCoat(String coat) {
        this.coat = coat;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPhotos(Photos[] photos) {
        this.photos = photos;
    }

    public void setCroppedPhoto(Photos croppedPhoto) {
        this.croppedPhoto = croppedPhoto;
    }

    public void setColors(Colors colors) {
        this.colors = colors;
    }

    public void setBreeds(Breeds breeds) {
        this.breeds = breeds;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }
}
