package com.example.petfinder4goon.Model.POJO;

import com.google.gson.annotations.SerializedName;

public class Type {
    @SerializedName("name")
    private String name;
    @SerializedName("coats")
    private String[] coats;
    @SerializedName("colors")
    private String[] colors;
    @SerializedName("genders")
    private String[] genders;

    public String getName() {
        return name;
    }

    public String[] getCoats() {
        return coats;
    }

    public String[] getColors() {
        return colors;
    }

    public String[] getGenders() {
        return genders;
    }
}
