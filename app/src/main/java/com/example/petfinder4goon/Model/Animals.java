package com.example.petfinder4goon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Animals {
    @SerializedName("animals")
    private List<AnimalItem> animalItems;

    public List<AnimalItem> getAnimalItems() {
        return animalItems;
    }

    public void setAnimalItems(List<AnimalItem> animalItems) {
        this.animalItems = animalItems;
    }
}
