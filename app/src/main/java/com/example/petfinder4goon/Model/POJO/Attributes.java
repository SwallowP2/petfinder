package com.example.petfinder4goon.Model.POJO;

import android.text.BoringLayout;

import com.google.gson.annotations.SerializedName;

public class Attributes {
    @SerializedName("spayed_neutered")
    private Boolean spayed_neutered;
    @SerializedName("house_trained")
    private Boolean house_trained;
    @SerializedName("declawed")
    private Boolean declawed;
    @SerializedName("special_needs")
    private Boolean special_needs;
    @SerializedName("shots_current")
    private Boolean shots_current;

    public Boolean getDeclawed() {
        return declawed;
    }
    public Boolean getHouse_trained() {
        return house_trained;
    }

    public Boolean getShots_current() {
        return shots_current;
    }

    public Boolean getSpayed_neutered() {
        return spayed_neutered;
    }

    public Boolean getSpecial_needs() {
        return special_needs;
    }
}
