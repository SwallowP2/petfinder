package com.example.petfinder4goon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OneAnimal {
    @SerializedName("animal")
    private AnimalItem animalItems;

    public AnimalItem getAnimalItems() {
        return animalItems;
    }

    public void setAnimalItems(AnimalItem animalItems) {
        this.animalItems = animalItems;
    }
}
