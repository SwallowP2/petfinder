package com.example.petfinder4goon.Model.POJO;

import com.google.gson.annotations.SerializedName;

import retrofit2.http.Url;

public class Photos {
    @SerializedName("medium")
    private String meduim;
    @SerializedName("small")
    private String small;
    @SerializedName("large")
    private String large;
    @SerializedName("full")
    private String full;

    public Photos(String full) {
        this.full = full;
    }

    public Photos(String meduim, String small, String large, String full) {
        this.meduim = meduim;
        this.small = small;
        this.large = large;
        this.full = full;
    }

    public void setMeduim(String meduim) {
        this.meduim = meduim;
    }

    public String getMeduim() {
        return meduim;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }
}

