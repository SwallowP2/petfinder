package com.example.petfinder4goon.Model.POJO;

import com.google.gson.annotations.SerializedName;

public class Colors {
    @SerializedName("primary")
    String primaryColor;
    @SerializedName("secondary")
    String secondryColor;
    @SerializedName("tertiary")
    String tertiary;

    public String getPrimaryColor() {
        return primaryColor;
    }

    public String getSecondryColor() {
        return secondryColor;
    }

    public String getTertiary() {
        return tertiary;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public void setSecondryColor(String secondryColor) {
        this.secondryColor = secondryColor;
    }

    public void setTertiary(String tertiary) {
        this.tertiary = tertiary;
    }
}
