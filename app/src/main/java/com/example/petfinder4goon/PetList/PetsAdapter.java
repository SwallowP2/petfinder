package com.example.petfinder4goon.PetList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.petfinder4goon.LoginActivity;
import com.example.petfinder4goon.Model.AnimalItem;
import com.example.petfinder4goon.PetDetailsActivity;
import com.example.petfinder4goon.PetsRoomDataBase.PetsDataBase;
import com.example.petfinder4goon.PetsRoomDataBase.PetsDataBaseAccessor;
import com.example.petfinder4goon.R;
import com.example.petfinder4goon.Utilities.DownloadImageTask;
import com.example.petfinder4goon.Utilities.SharedPrefs;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

import static com.example.petfinder4goon.R.layout.cardview_animal;

public class PetsAdapter extends RecyclerView.Adapter<PetsAdapter.ViewHolder> {

    private LayoutInflater layoutInflater;
    private List<AnimalItem> data;
    private Context context;
    PetsDataBase petsDB;
    private int id;
    Boolean fromList;

    public PetsAdapter(Context context, List<AnimalItem> data, Boolean fromList){
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.fromList = fromList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView animalName, animalType, animalAge, animlStatus, animalSize;
        ImageView image;

        ViewHolder(@NonNull View itemView){
            super(itemView);
            animalName = itemView.findViewById(R.id.animal_name);
            animalType = itemView.findViewById(R.id.type_animal);
            animalAge = itemView.findViewById(R.id.animal_age);
            animlStatus = itemView.findViewById(R.id.animal_status);
            animalSize = itemView.findViewById(R.id.animal_size);
            image = itemView.findViewById(R.id.petimageView);

            itemView.setOnClickListener(v -> {
                id = data.get(getAdapterPosition()).getId();
                Intent intent = new Intent(context, PetDetailsActivity.class);
                intent.putExtra("id",id);
                intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            });

            if(fromList){
                itemView.setOnLongClickListener(v -> {
                    petsDB = PetsDataBaseAccessor.getInstance(context);
                    new MaterialAlertDialogBuilder(context, R.style.AlertDialogTheme)
                            .setTitle("Do you want to save this pet to pins?")
                            .setPositiveButton("yes", (dialogInterface, i) -> new Thread(() -> {
                                AnimalItem animal = data.get(getAdapterPosition());
                                petsDB.petsDao().insertPets(new AnimalItem(animal.getId(),animal.getName(),animal.getDescription(),
                                        animal.getAge(),animal.getType(),animal.getSize(),animal.getGender(),
                                        animal.getCoat(),animal.getStatus(),animal.getUrl()));
                            }) .start())
                            .setNegativeButton("no", (dialogInterface, i) -> dialogInterface.dismiss())
                            .show();

                    return false;
                });
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(cardview_animal,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.animalName.setText(data.get(position).getName());
        holder.animalType.setText(data.get(position).getType());
        holder.animalAge.setText(data.get(position).getAge());
        holder.animlStatus.setText(data.get(position).getStatus());
        holder.animalSize.setText(data.get(position).getSize());

        if(fromList && data.get(position).getPhotos().length > 0){
            String url = data.get(position).getCroppedPhoto().getMeduim();
            new DownloadImageTask(holder.image).execute(url);
        }
        else if(fromList && data.get(position).getPhotos().length == 0){
            holder.image.setImageResource(R.drawable.ic_launcher_foreground);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
