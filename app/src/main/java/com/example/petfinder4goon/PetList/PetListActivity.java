package com.example.petfinder4goon.PetList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.petfinder4goon.Api.ApiClient;
import com.example.petfinder4goon.Api.ApiService;
import com.example.petfinder4goon.Api.tokenResponse;
import com.example.petfinder4goon.FilterPetsActivity;
import com.example.petfinder4goon.LoginActivity;
import com.example.petfinder4goon.Model.AnimalItem;
import com.example.petfinder4goon.Model.Animals;
import com.example.petfinder4goon.Model.POJO.Breeds;
import com.example.petfinder4goon.Model.POJO.Colors;
import com.example.petfinder4goon.Model.POJO.Photos;
import com.example.petfinder4goon.R;
import com.example.petfinder4goon.SavedPetsActivity;
import com.example.petfinder4goon.Utilities.SharedPrefs;
import com.example.petfinder4goon.Utilities.Utilitis;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PetListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    PetsAdapter adapter;
    List<AnimalItem> animals;
    Toolbar toolbar;
    private DrawerLayout mDrawer;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_list);

        toolbar = findViewById(R.id.toolbar_petlist);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawer = findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();

        // Setup toggle to display hamburger icon with nice animation
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawer.addDrawerListener(drawerToggle);
        nvDrawer = findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);

        // method to check if the token is valid
        Utilitis.RefreshToken();
        GetAnimals();

        recyclerView = findViewById(R.id.recyclerView);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()){
                        case R.id.to_exit:
                            new MaterialAlertDialogBuilder(PetListActivity.this, R.style.AlertDialogTheme)
                                    .setTitle("Do you want to logout?")
                                    .setPositiveButton("yes", (dialogInterface, i) -> {
                                        SharedPrefs.instance().clearPref();
                                        Intent intent = new Intent(PetListActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    })
                                    .setNegativeButton("no", (dialogInterface, i) -> dialogInterface.dismiss())
                                    .show();
                            break;
                        case R.id.to_filter:
                            Intent intent = new Intent(PetListActivity.this, FilterPetsActivity.class);
                            startActivity(intent);
                            break;

                        case R.id.to_pin:
                            intent = new Intent(PetListActivity.this, SavedPetsActivity.class);
                            startActivity(intent);
                            break;

                    }
                    return true;
                });
    }
    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }


    private void GetAnimals(){
        LinearLayout progressBar = findViewById(R.id.llProgressBar_petlist);
        progressBar.setVisibility(View.VISIBLE);
        ApiService api = ApiClient.getClient().create(ApiService.class);
        Call<Animals> call = api.getAnimals("Bearer " + SharedPrefs.instance().fetchValueString("token"),
                                            null,null,null,null,null, null, null);
        call.enqueue(new Callback<Animals>() {
            @Override
            public void onResponse(Call<Animals> call, Response<Animals> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    //Toast.makeText(PetListActivity.this, "successful", Toast.LENGTH_SHORT).show();
                    Animals ResponseJson = response.body();

                    animals = ResponseJson.getAnimalItems();
                    adapter = new PetsAdapter(PetListActivity.this, animals, true);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(PetListActivity.this));

                } else {
                    progressBar.setVisibility(View.GONE);
                    // TODO: correct toasts
                    Toast.makeText(PetListActivity.this,String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Animals> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "مشکل در برقراری ارتباط", Toast.LENGTH_SHORT).show();
            }
        });
    }
}