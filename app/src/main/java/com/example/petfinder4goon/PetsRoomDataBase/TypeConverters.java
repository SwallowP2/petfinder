package com.example.petfinder4goon.PetsRoomDataBase;

import androidx.room.TypeConverter;

import com.example.petfinder4goon.Model.POJO.Photos;

public class TypeConverters {
    @TypeConverter
    public static Photos photoFromDB(String full) {
        return full == null ? null : new Photos(full);
    }
    @TypeConverter
    public static String photoToDB(Photos photos) {
        return photos == null ? null : photos.getFull();
    }
}
