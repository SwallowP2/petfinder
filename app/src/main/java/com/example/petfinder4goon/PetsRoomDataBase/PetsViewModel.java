package com.example.petfinder4goon.PetsRoomDataBase;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.petfinder4goon.Model.AnimalItem;

import java.util.List;

import io.reactivex.Maybe;

public class PetsViewModel extends AndroidViewModel {
    private PetsRepository respository;
    private LiveData<List<AnimalItem>> pets;

    public PetsViewModel(@NonNull Application application) {
        super(application);
        respository = new PetsRepository(application);
        pets = respository.getAll();
    }

    public void insert(AnimalItem animalItem){
        respository.insert(animalItem);
    }

    public LiveData<List<AnimalItem>> getAll(){
        return pets;
    }
}
