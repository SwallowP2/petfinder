package com.example.petfinder4goon.PetsRoomDataBase;

import android.app.Application;
import android.app.ListActivity;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.petfinder4goon.Model.AnimalItem;

import java.util.List;


public class PetsRepository {
    private PetsDao petsDao;
    private LiveData<List<AnimalItem>> pets;

    public PetsRepository(Application application) {
        PetsDataBase db = PetsDataBaseAccessor.getInstance(application);
        petsDao = db.petsDao();
        pets = petsDao.getAll();
    }

    public void insert(AnimalItem animalItem){
        new Thread(() -> petsDao.insertPets(animalItem)).start();

    }

    public LiveData<List<AnimalItem>> getAll(){
        return pets;
    }

}
