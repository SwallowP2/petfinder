package com.example.petfinder4goon.PetsRoomDataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.petfinder4goon.Model.AnimalItem;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface PetsDao {
    @Query("SELECT * FROM AnimalItem")
    public LiveData<List<AnimalItem>> getAll();

    @Insert
    void insertAll(AnimalItem... animalItems);

    @Insert
    void insert(AnimalItem animalItems);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertPets(AnimalItem... animalItems);
}
