package com.example.petfinder4goon.PetsRoomDataBase;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class PetsDataBaseAccessor {
    private static PetsDataBase PetsDatabaseInstance;
    private static final String PETS_DB_NAME = "pets_db";
    private PetsDataBaseAccessor() {}
    public static PetsDataBase getInstance(Context context) {
        if (PetsDatabaseInstance == null) {
// Create or open a new SQLite database, and return it as
// a Room Database instance.
            PetsDatabaseInstance = Room.databaseBuilder(context,
                    PetsDataBase.class, PETS_DB_NAME).fallbackToDestructiveMigration().addCallback(roomCallBack).build();
        }
        return PetsDatabaseInstance;
    }

    private static RoomDatabase.Callback roomCallBack = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };
}
