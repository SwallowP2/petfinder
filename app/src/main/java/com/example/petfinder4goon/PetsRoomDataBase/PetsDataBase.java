package com.example.petfinder4goon.PetsRoomDataBase;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.petfinder4goon.Model.AnimalItem;

@Database(entities = {AnimalItem.class}, version = 1, exportSchema = false)
public abstract class PetsDataBase extends RoomDatabase {
    public abstract PetsDao petsDao();
}
