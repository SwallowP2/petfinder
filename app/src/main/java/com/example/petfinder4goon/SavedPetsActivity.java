package com.example.petfinder4goon;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.petfinder4goon.Model.AnimalItem;
import com.example.petfinder4goon.PetList.PetListActivity;
import com.example.petfinder4goon.PetList.PetsAdapter;
import com.example.petfinder4goon.PetsRoomDataBase.PetsDataBase;
import com.example.petfinder4goon.PetsRoomDataBase.PetsDataBaseAccessor;
import com.example.petfinder4goon.PetsRoomDataBase.PetsViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;

public class SavedPetsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    PetsAdapter adapter;
    List<AnimalItem> animals = new ArrayList<>();
    PetsDataBase petsDB;
    private PetsViewModel petsViewModel;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_pets);

        recyclerView = findViewById(R.id.savedpets_recyclerview);

        petsViewModel =  new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication())).get(PetsViewModel.class);

        petsViewModel.getAll().observe(this, animalItems -> {
            //update recyclerview
            adapter = new PetsAdapter(SavedPetsActivity.this, animalItems,false);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(SavedPetsActivity.this));
        });


        toolbar = findViewById(R.id.toolbar_petDetails);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        TextView tv = findViewById(R.id.toolbar_text);
        tv.setText(R.string.pined_pets);
        ImageButton btn = findViewById(R.id.filter_backbutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavedPetsActivity.super.onBackPressed();
            }
        });

    }
}