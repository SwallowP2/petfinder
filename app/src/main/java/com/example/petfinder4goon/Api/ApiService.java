package com.example.petfinder4goon.Api;

import android.graphics.Color;

import com.example.petfinder4goon.Model.AnimalItem;
import com.example.petfinder4goon.Model.AnimalTypes;
import com.example.petfinder4goon.Model.Animals;
import com.example.petfinder4goon.Model.OneAnimal;
import com.example.petfinder4goon.Model.POJO.Breeds;
import com.example.petfinder4goon.Model.POJO.Colors;
import com.example.petfinder4goon.Model.POJO.Type;
import com.example.petfinder4goon.Model.TypeBreeds;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @FormUrlEncoded
    @POST("oauth2/token")
            @Headers({
                    "Content-Type: application/x-www-form-urlencoded"})

    Call<ResponseBody> getToken (@Field("grant_type") String grant_type,
                                 @Field("client_id") String client_id,
                                 @Field("client_secret") String client_secret);

    @GET("animals")
    Call<Animals> getAnimals (@Header("Authorization") String token,
                              @Query("name") String name,
                              @Query("type") String type,
                              @Query("gender") String gender,
                              @Query("age") String age,
                              @Query("size") String size,
                              @Query("color") String color,
                              @Query("breed") String breed
                              );

    @GET("animals/{id}")
    Call<OneAnimal> getAnimal (@Header("Authorization") String token, @Path("id") int id);

    @GET("types")
    Call<AnimalTypes> getTypes (@Header("Authorization") String token);

    @GET("types/{type}/breeds")
    Call<TypeBreeds> getBreeds (@Header("Authorization") String token, @Path("type") String type);
}
