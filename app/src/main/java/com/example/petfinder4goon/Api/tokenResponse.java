package com.example.petfinder4goon.Api;

import com.google.gson.annotations.SerializedName;

public class tokenResponse {
    @SerializedName("access_token")
    private String acccess_token;
    @SerializedName("token_type")
    private String token_type;
    @SerializedName("expires_in")
    private int expires_in;

    private long creationTime;

    public tokenResponse(){

    }

    public tokenResponse(String acccess_token, String token_type, int expires_in){
        this.acccess_token = acccess_token;
        this.token_type = token_type;
        this.expires_in = expires_in;
    }

    public String getAcccess_token() {
        return acccess_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setAcccess_token(String acccess_token) {
        this.acccess_token = acccess_token;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }
}
