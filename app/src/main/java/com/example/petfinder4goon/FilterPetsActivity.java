package com.example.petfinder4goon;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petfinder4goon.Api.ApiClient;
import com.example.petfinder4goon.Api.ApiService;
import com.example.petfinder4goon.Model.AnimalItem;
import com.example.petfinder4goon.Model.AnimalTypes;
import com.example.petfinder4goon.Model.Animals;
import com.example.petfinder4goon.Model.POJO.Type;
import com.example.petfinder4goon.Model.TypeBreeds;
import com.example.petfinder4goon.PetList.PetListActivity;
import com.example.petfinder4goon.PetList.PetsAdapter;
import com.example.petfinder4goon.Utilities.SharedPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterPetsActivity extends AppCompatActivity {

    TextView nameTextView;
    RecyclerView recyclerView;
    PetsAdapter adapter;
    Button searchBtn;
    List<AnimalItem> animals;
    Boolean typeChosen = false;
    String name,gender,type,age,size,color,breed;
    Spinner typeSpinner, ageSpinner, sizeSpinner, genderSpinner, colorSpinner, breedSpinner;
    ArrayList<String> genders_,colors_,types_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_pets);

        nameTextView = findViewById(R.id.s_name);
        genderSpinner = findViewById(R.id.s_gender);
        sizeSpinner = findViewById(R.id.s_size);
        typeSpinner = findViewById(R.id.s_type);
        ageSpinner = findViewById(R.id.s_age);
        breedSpinner = findViewById(R.id.s_breed);
        colorSpinner = findViewById(R.id.s_color);
        recyclerView = findViewById(R.id.filter_recyclerview);
        searchBtn = findViewById(R.id.search_button);

        ImageButton backBtn = findViewById(R.id.filter_backbutton);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterPetsActivity.super.onBackPressed();
            }
        });


        GetTypes();

        String[] ages = {"","baby","adult","young","senior"};
        ageSpinner.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,ages));

        String[] sizes = {"","small","medium","large"};
        sizeSpinner.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,sizes));



        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    GetAnimals();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void GetAnimals(){
        LinearLayout progressBar = findViewById(R.id.llProgressBar_petfilter);
        progressBar.setVisibility(View.VISIBLE);
        ApiService api = ApiClient.getClient().create(ApiService.class);
        TextView noResTV = findViewById(R.id.no_results_tv);

        name = nameTextView.getText().toString();
        type = typeSpinner.getSelectedItem().toString();
        gender = genderSpinner.getSelectedItem().toString();
        age = ageSpinner.getSelectedItem().toString();
        size = sizeSpinner.getSelectedItem().toString();
        color = colorSpinner.getSelectedItem().toString();


        if(typeChosen){
            breed = breedSpinner.getSelectedItem().toString();
        } else {
            breed = null;
            breedSpinner.setAdapter(new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, new String[]{""}));
        }

        Call<Animals> call = api.getAnimals("Bearer " + SharedPrefs.instance().fetchValueString("token"),
                EmptyToNull(name),EmptyToNull(type),EmptyToNull(gender),EmptyToNull(age),EmptyToNull(size),EmptyToNull(color),breed);
        call.enqueue(new Callback<Animals>() {
            @Override
            public void onResponse(Call<Animals> call, Response<Animals> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.INVISIBLE);
                    noResTV.setVisibility(View.GONE);
                    //Toast.makeText(PetListActivity.this, "successful", Toast.LENGTH_SHORT).show();
                    Animals ResponseJson = response.body();

                    animals = ResponseJson.getAnimalItems();

                    Log.v("animals",String.valueOf(animals.size()));
                    if(animals.size() == 0){
                        noResTV.setVisibility(View.VISIBLE);
                    }

                        adapter = new PetsAdapter(FilterPetsActivity.this, animals, true);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(FilterPetsActivity.this));

                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(FilterPetsActivity.this,R.string.no_resuts, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Animals> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "مشکل در برقراری ارتباط", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList GetTypes(){
        ArrayList<String> types_ = new ArrayList<>();

        ApiService api = ApiClient.getClient().create(ApiService.class);
        Call<AnimalTypes> call = api.getTypes("Bearer " + SharedPrefs.instance().fetchValueString("token"));
        call.enqueue(new Callback<AnimalTypes>() {
            @Override
            public void onResponse(Call<AnimalTypes> call, Response<AnimalTypes> response) {
                if(response.isSuccessful()){
                    AnimalTypes resp = response.body();

                    types_.add(0, "");
                    for(int i = 0; i < resp.getTypes().length ; i++){
                        types_.add(i+1, resp.getTypes()[i].getName());
                    }

                    String[] types = new String[resp.getTypes().length+1];
                    try {
                        ArrayAdapter typeAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,types_.toArray(types));
                        typeSpinner.setAdapter(typeAdapter);

                        typeChosen = true;

                        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                ArrayList<String> colors_ = new ArrayList<>();
                                ArrayList<String> genders_ = new ArrayList<>();

                                colors_.add(0, "");
                                String[] colors = new String[resp.getTypes()[position].getColors().length+1];
                                for(int i = 0; i < resp.getTypes()[position].getColors().length ; i++){
                                    colors_.add(i+1,resp.getTypes()[position].getColors()[i]);
                                }

                                genders_.add(0, "");
                                String[] genders = new String[resp.getTypes()[position].getGenders().length+1];
                                for(int i = 0; i < resp.getTypes()[position].getGenders().length ; i++){
                                    genders_.add(i+1,resp.getTypes()[position].getGenders()[i]);
                                }

                                genderSpinner.setAdapter(new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,
                                        genders_.toArray(genders)));

                                colorSpinner.setAdapter(new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,
                                        colors_.toArray(colors)));

                                GetBreeds(types_.get(position));

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(FilterPetsActivity.this, R.string.something_went_wrong + " get types", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AnimalTypes> call, Throwable t) {
                Toast.makeText(FilterPetsActivity.this, R.string.something_went_wrong + " failed", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
        Log.v("types", String.valueOf(types_.size()));
        return types_;
    }

    private void GetBreeds(String type){

        if(type==null || type.isEmpty()){
            type = "Dogs";
        }
        ArrayList<String> breeds = new ArrayList<>();
        ApiService api = ApiClient.getClient().create(ApiService.class);
        Call<TypeBreeds> call = api.getBreeds("Bearer " + SharedPrefs.instance().fetchValueString("token"), type);
        call.enqueue(new Callback<TypeBreeds>() {
            @Override
            public void onResponse(Call<TypeBreeds> call, Response<TypeBreeds> response) {
                if(response.isSuccessful()){
                    TypeBreeds resp = response.body();

                    breeds.add(0, "");
                    for(int i = 0; i < resp.getBreeds().length ; i++){
                        breeds.add(i+1, resp.getBreeds()[i].getName());
                    }
                    Log.v("breeds",breeds.get(0));

                    String[] breeds_ = new String[resp.getBreeds().length+1];
                    breedSpinner.setAdapter(new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, breeds.toArray(breeds_)));

                }
                else{
                    //Toast.makeText(FilterPetsActivity.this, R.string.something_went_wrong + "breed resp", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TypeBreeds> call, Throwable t) {
                Toast.makeText(FilterPetsActivity.this, R.string.connection_problem + " breed fail", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private String EmptyToNull(String str){
        if(str.isEmpty()){
            return null;
        } else{
            return str;
        }
    }
}